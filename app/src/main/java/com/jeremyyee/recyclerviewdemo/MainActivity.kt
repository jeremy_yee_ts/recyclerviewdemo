package com.jeremyyee.recyclerviewdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val animalsList: ArrayList<Animal> = ArrayList()
        animalsList.add(Animal("Anteater"))
        animalsList.add(Animal("Beaver"))
        animalsList.add(Animal("Chicken"))
        animalsList.add(Animal("Cow"))
        animalsList.add(Animal("Duck"))

        mainRecyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = AnimalAdapter(this)
        mainRecyclerView.adapter = adapter
        adapter.setAnimals(animalsList)
        adapter.notifyDataSetChanged()
    }
}

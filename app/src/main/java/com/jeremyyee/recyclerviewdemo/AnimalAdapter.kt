package com.jeremyyee.recyclerviewdemo

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.holder_alphabet.view.*
import kotlinx.android.synthetic.main.holder_animal.view.*

class AnimalAdapter(private val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val animalType: Int = 0
    private val alphabetType: Int = 1
    private var data: ArrayList<Any> = ArrayList()

    fun setAnimals(animals: ArrayList<Animal>) {
        this.data.clear()
        val alphabets: ArrayList<String> = arrayListOf("A", "B", "C", "D", "E")
        for (alphabet in alphabets) {
            this.data.add(alphabet)
            val filteredAnimals: List<Animal> = animals.filter { it.name.startsWith(alphabet, true) }
            filteredAnimals.forEach { this.data.add(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == animalType) {
            AnimalViewHolder(LayoutInflater.from(context).inflate(R.layout.holder_animal, parent, false))
        } else {
            AlphabetViewHolder(LayoutInflater.from(context).inflate(R.layout.holder_alphabet, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return this.data.size
    }

    override fun getItemViewType(position: Int): Int {
        val item: Any = this.data[position]
        return if (item is Animal) animalType else alphabetType
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val type = getItemViewType(position)
        if (type == animalType) {
            val animalViewHolder: AnimalViewHolder = holder as AnimalViewHolder
            animalViewHolder.setAnimal(data[position] as Animal)
        } else if (type == alphabetType) {
            val alphabetViewHolder: AlphabetViewHolder = holder as AlphabetViewHolder
            alphabetViewHolder.setTitle(data[position] as String)
        }
    }

}

class AnimalViewHolder(view: View): RecyclerView.ViewHolder(view) {
    private val animalNameTextView: TextView = view.animalTextView

    fun setAnimal(animal: Animal) {
        animalNameTextView.text = animal.name
    }
}

class AlphabetViewHolder(view: View): RecyclerView.ViewHolder(view) {
    private val alphabetTitleTextView: TextView = view.alphabetTextView

    fun setTitle(title: String) {
        alphabetTitleTextView.text = title
    }
}

class Animal(val name: String)